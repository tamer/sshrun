[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

sshrun command
==============
Send commands to remote ssh servers

```bash
$ sshrun -h
  -cmd string
    	command to run on remote host (default "ls -Alh")
  -d	Verbose/debug mode
  -h	Display help
  -host string
    	Remote host name
  -pass string
    	Remote host user password, leave blank for ssh key login
  -post int
    	Remote port address (default 22)
  -user string
    	Remote host user name
```

```bash
sshrun -host example.net -user username -pass ***** -cmd 'df -h'
```
