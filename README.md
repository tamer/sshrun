[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![GoDoc](https://godoc.org/git.hansaray.pw/go/sshrun?status.png)](https://godoc.org/git.hansaray.pw/go/sshrun) [![Go Report Card](https://goreportcard.com/badge/git.hansaray.pw/go/sshrun)](https://goreportcard.com/report/git.hansaray.pw/go/sshrun)


## Send commands to remote ssh servers library

This is a fork and some improvements of [jujun/dsfsdfsdf](https://codeberg.org/jujun/dsfsdfsdf), which has been removed since.

```bash
$ sshrun -h
  -cmd string
    	command to run on remote host (default "ls -Alh")
  -d	Verbose/debug mode
  -h	Display help
  -host string
    	Remote host name
  -pass string
    	Remote host user password, leave blank for ssh key login
  -post int
    	Remote port address (default 22)
  -user string
    	Remote host user name

```
```bash
sshrun -host example.net -user username -pass ***** -cmd 'df -h'
```
